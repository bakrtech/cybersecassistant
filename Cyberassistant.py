from backend.VoicePart  import *
from backend.nmapscaner import *
from backend.passwordmanager import *
from backend.Jokes import Jokes
import os
import random
def network_scaner():
    speak("Scaning the network!!!")
    a= Network_scan()
    speak(a)
    return "done"
def BrowserLauncher():
   speak("Which profile of browser you want: Low Security Medium Security High Security ")
   said=listener().lower()
   if 'low' in said:
       os.system("firefox --profile backend/firefoxProfiles/NoSec/&exit")
   elif 'medium' in said:
       os.system("firefox --profile backend/firefoxProfiles/MediumProfile/&exit")
   elif 'high' in said:
       os.system('tor-browser&exit')
   else:
       speak('Can not understod what you said ! can you speak again plz')
   return "done"
def ChatBot(said):
    if 'scan the network' in said:
        output =network_scaner()
        return output
    #Browser launching 
    elif 'browser' in said:
        output=BrowserLauncher()
        return output
    elif 'help'in said:
        return("I am in development !!! ")
    #wikipedia searching
    elif 'flip' in said and 'coin' in said:
        choice = ["Heads","Tails"]
        return(choice[random.randint(0,1)])
    elif 'the time'in said:
        time = datetime.datetime.now().strftime("%H:%M:%S")
        return f"The time is {time}"
    elif  "name" in said and "your" in said:
        return "My name is --------------"
    elif "password" in said and "genrate" in said:
        speak("Please see the terminal to process further")
        password_manager()
    elif "Who created you" in said:

        return "My creation was a teamwork, so I can't take a specific name."
    elif ("Tell me a joke" in said or "say a joke" in said or "say something funny" in said or "make me laugh" in said):
        return random.choice(Jokes)
    else:
        return "Input not recogonized!!"
if __name__ == '__main__':
    speak('Welcome to CybersecAssistant , How may I help you  ')
    choice = str(input("How do you want to intract Voice(Type V) or Typing the Input(Type K)"))
    while choice=='V'or choice=='Voice':
       said = listener().lower()
       if said == 'none' or 'quit now' in said or 'exit'in said :
           speak("Are you sure you want to exit")
           said = listener().lower()
           if  'yes' in said or said =="none":
               speak("I am quitting ")
               break
        #Scaning the network using nmap
       output =ChatBot(said)
       speak(output)
    while choice=='K'or choice=='Keyboard':
        said=str(input("> ")).lower()
        if said == 'none' or 'quit' in said or 'exit'in said :
           speak("Are you sure you want to exit")
           print("Are you sure you want to exit")
           if 'yes' in said:
               speak("I am quitting")
               print("I am quitting")
               break
        output= ChatBot(said)
        speak(output)
