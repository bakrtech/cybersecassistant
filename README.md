# CYBERSEC AI
## Features
- Network scaning
- Different browser profiles for different work cenarios
- Password genrator
- Basic AI features without telementry

## Requirement
- Python latest version (3.10+ )
- `pip` 
- audio drivers

## Structure of files 
```shell
├── backend
│   ├── CybersecAssistant.log
│   ├── Jokes.py
│   ├── nmapscaner
│   │   ├── __init__.py
│   │   ├── nmapscan.py
│   │   ├── __pycache__
│   │   │   ├── __init__.cpython-310.pyc
│   │   │   ├── nmapscan.cpython-310.pyc
│   │   │   └── reader.cpython-310.pyc
│   │   └── reader.py
│   ├── passwordmanager
│   │   ├── __init__.py
│   │   ├── password_filesearch.py
│   │   ├── passwordgenerator.py
│   │   ├── passwords.dat
│   │   └── __pycache__
│   │       ├── __init__.cpython-310.pyc
│   │       └── passwordgenerator.cpython-310.pyc
│   ├── popup.py
│   └── VoicePart
│       ├── __init__.py
│       ├── __pycache__
│       │   ├── __init__.cpython-310.pyc
│       │   └── voiceRecog.cpython-310.pyc
│       └── voiceRecog.py
├── Cyberassistant.py
├── CybersecAssistant.log
├── LICENSE
├── README.md
├── requirements.txt
├── tempfolder
│   ├── nmapoutput.txt
│   └── passwords.dat
└── todolist
```

## Installation
### GNU/Linux & MAC
```shell
git clone https://gitlab.com/bakrtech/cybersecassistant
cd cybersecassistant
pip install -r requirements.txt
python Cybersecassistant.py
```

### Windows
- Download the .zip file from the https://gitlab.com/bakrtech/cybersecassistant
- Unzip the .zip file
- open powershell
```powershell
pip install -r requirements.txt
python Cybersecassistant.py
```

## Contrubuting
[Lazer](https://gitlab.com/mayankgurjar3112) [Ameya Shandilya](https://gitlab.com/ameyashandilya7) [Mr.Sagittatrius](https://gitlab.com/yashmeena9274) 

## License
GNU v2 
