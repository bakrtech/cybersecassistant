import os,re

#globals
ipv4 = []
ipv6 = []
port = []
ipdata = dict()

def set_path():
    '''To set cwd to tempfolder of AI'''
    cwd = __file__.replace(os.path.basename(__file__),"")
    os.chdir(cwd)
    os.chdir("../../")
    os.chdir("tempfolder")

set_path()
file = open("nmapoutput.txt","r")
t = file.readlines()
lst = []
cnt = 0

def ya(a, b):
    '''To reset list while scan for dict'''
    if a != 0:
        lst.append(a)
    global cnt
    cnt = len(lst)
    if b != 0:
        return cnt
    
def portscan4dic(d):
    b = []
    d = d.split(" ")
    port_std = re.compile(r'(\d{1,5})')
    for data in d:
        if "/tcp" in data or "/udp" in data:
            if port_std.search(data) != None:
                x = port_std.search(data)[0]
                if x + "/tcp" in data:
                    b.append(x + "/tcp")
                if x + "/udp" in data:
                    b.append(x + "/udp")
    return b

def dic_key():
    '''Opened ports of ip'''
    y = None
    a = 0
    z = []

    ipv4_std = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
    for line in t:
        x = ipv4_std.search(line)
        if x != None:
            y = "Nmap scan report for " + x[0]
            if y not in ipdata.keys() and y + "\n" in t:
                ipdata["Nmap scan report for " + x[0]] = " "
                ya(y,0)
        if "Nmap scan report for " not in line and y != None and len(ipdata.keys()) != 0:
            z += portscan4dic(line)
            if z != []:
                ipdata[y] = z
            if a != cnt:
                a = ya(0,1)
                z = []
    return ipdata      

#__main__
a = dic_key()
print(a)
