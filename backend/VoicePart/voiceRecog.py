

#IMPORTING STUFF
import datetime,pyttsx3,sys , wikipedia , logging  
import speech_recognition as sr

#Loging setup 
logging.basicConfig(filename="CybersecAssistant.log", level=logging.INFO)

#Gettting OperatingSystem Information
def get_platform():
    platforms = {
        'linux1': 'Linux',
        'linux2': 'Linux',
        'darwin': 'OS X',
        'win32': 'Windows',
        'win64':'Windows'
    }
    if sys.platform not in platforms:
        return sys.platform

    return platforms[sys.platform]
os = get_platform()

#Different voice engines for diffrent OSs
if os =='linux':
    engine =pyttsx3.init('espeak')
    voices = engine.getProperty('voices')
    print(f"Using the {voices[11].id} for speaking !")
    engine.setProperty('voice',voices[11].id)
elif os =='OS X':
    engine =pyttsx3.init('nsss')
    voices = engine.getProperty('voices')
    print(f"Using the {voices[2].id} for speaking !")
    engine.setProperty('voice',voices[2].id)
else:
    engine = pyttsx3.init('sapi5')
    voices = engine.getProperty('voices')
    print(f"Using the {voices[1].id} for speaking !")
    engine.setProperty('voice',voices[1].id)
voices = engine.getProperty('voices')

#Speaking engine setup(Text to speech )

def speak(audio):
    engine.say(audio)
    engine.runAndWait()

#Listening Engine setup 

def listener():
    r = sr.Recognizer()
    r.energy_threshold = 1568 
    r.dynamic_energy_threshold = True
    with sr.Microphone() as source:
        print("listening")
        r.pause_threshold =1
        audio = r.listen(source)
 #Trying to understand the the voice (Speach to text)   
    try:
        print("Trying to understand....")
        query =r.recognize_google(audio,language='en-in')
        print(f"user said: {query}\n")
        return query
    
    except Exception as e:
        time =  datetime.datetime.now().strftime("%H:%M:%S")
        logging.info(f"{time}>>{e}")
        print("Say that agin please....")
        return "None"



