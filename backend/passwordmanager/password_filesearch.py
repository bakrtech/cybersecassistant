import pickle
import ast

cwd = __file__.replace(os.path.basename(__file__),"")
os.chdir(cwd)
data = str()
pass_name = input("Enter password name: ")
f = open("passwords.dat","rb")

while True:
    try:
        data += pickle.load(f)
    except EOFError:
        break

data = data.split("\n")

for i in range(len(data)):
    if pass_name in data[i]:
        datum = ast.literal_eval(data[i])
        p = str(datum.values())
        q = p.lstrip("datum.keys()")
        r = p.rstrip("'])")
print("Password of ",pass_name," is: ",p)
