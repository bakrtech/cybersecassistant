import random
import os
import array
import pickle
 
def set_path():
    '''To set cwd to tempfolder of AI'''
    cwd = __file__.replace(os.path.basename(__file__),"")
    os.chdir(cwd)
    os.chdir("../")

# set_path()

 
 
def password_manager():
    DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] 
    LOCASE_CHARACTERS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    UPCASE_CHARACTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    SYMBOLS = ['@', '#', '$', '%', '=', ':', '?', '.', '/', '|', '~', '>', '*', '(', ')', '<']
    COMBINED_LIST = DIGITS + UPCASE_CHARACTERS + LOCASE_CHARACTERS + SYMBOLS
    
    rand_digit = random.choice(DIGITS)
    rand_upper = random.choice(UPCASE_CHARACTERS)
    rand_lower = random.choice(LOCASE_CHARACTERS)
    rand_symbol = random.choice(SYMBOLS)
    
    temp_pass = rand_digit + rand_upper + rand_lower + rand_symbol
    temp_pass_list = []
    
    MAX_LEN = int(input("Enter the length of the password: "))
    for x in range(MAX_LEN - 4):
        temp_pass = temp_pass + random.choice(COMBINED_LIST)
        temp_pass_list = array.array('u', temp_pass)
        random.shuffle(temp_pass_list)
    
    password = ""
    for x in temp_pass_list:
            password = password + x
             
    print(password)
    
    chc = input("Do you want to save this password? [(Yes/Y)/(No/N)]\n")
    
    if chc in ["yes","Yes","y","Y"]:
        pswd = open("passwords.dat","ab")
        pass_name = input("Name of the password: ")
        data = {pass_name:password}
        pickle.dump(data, pswd)
        pswd.close()
